package main

import (
	"fmt"
)

func main() {
	numberArray := [...]int{1, 2, 3, 45, 6, 78}
	fmt.Println(numberArray)
	fmt.Println(len(numberArray)) //获取数组长度
	fmt.Println(cap(numberArray)) //获取数组容量
	numberSlice := numberArray[:3]
	fmt.Println(numberSlice)
	fmt.Println(len(numberSlice)) //获取切片长度
	fmt.Println(cap(numberSlice)) //获取切片长度容量

}
