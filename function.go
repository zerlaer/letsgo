package main

import (
	"fmt"
	"math/rand"
)

//函数让程序模块化，实现代码复用，实现程序之间和程序与系统之间的相互调用和交流。

// addNum求和
func addNum(a int, b int) int {
	return a + b //返回 a 和 b的和
}

// 生成随机数
func randNum(n int) int {
	num := rand.Intn(n) //生成一个n之内的随机数
	return num
}

// 温度转换
func kelvinToCelsius(k float64) float64 {
	k -= 273.15
	return k
}

func main() {
	fmt.Println(addNum(1, 2)) //3
	fmt.Println(randNum(100)) //81
	kelvin := 298.0
	celsius := kelvinToCelsius(kelvin)
	fmt.Printf("%v°K 等于 %v°C", kelvin, celsius) //298°K 等于 24.850000000000023°C
}
